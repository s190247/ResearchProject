# Simulation study on perturbations in the sound zone problem
This repository includes the code that was used to model the sound zone problem as desribed in the paper *SimulationStudyOnPerturbationsInTheSoundZoneProblem.pdf*, which can also be found in this repository. Each of the simulations found in the paper have their own testscript. 

- **testscriptTemperature.m** contains the script to model the influence of temperature on the acoustic contrast
- **testscriptPosition.m** contains the script to model the influence of position on the acoustic contrast
- **testcriptDamping.m** contains the script to model the influence of position on the acoustic contrast
- **testscriptNoisy.m** contains the script to model the influence of noise on the acoustic contrast
- **testscriptLoudspeaker.m** contains the script to model the influence of speaker transfer functions on the acoustic contrast