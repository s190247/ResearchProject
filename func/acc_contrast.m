function [acc] = acousticcontrast(pB, pD)
%Calculates accoustic contrast according to equation 21 if pB and pD both
%have the format of p(m, f) where m is the number of microphones in the
%respective zone and f is the number of frequency bins.
% pB_l2 = sqrt(sum(abs(pB.^2),1));
% pD_l2 = sqrt(sum(abs(pD.^2),1));
% acc = pB_l2.^2 ./ pD_l2.^2;

MB = size(pB,1);
MD = size(pD,1);

acc = ( MB^-1 * sum(abs(pB.^2),1)) ./ (MD^-1 * sum(abs(pD.^2),1)); 
end

