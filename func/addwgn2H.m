function [HNoise, Noise] = addwgn2H(H, SNR, coherent)
%ADDWGN2H adds white gaussian noise with a given SNR to the Transfer
%function H by calculating the FFT of the sum of the RIR and the noise. The
%noise added to the microphones is scaled to achieve a given SNR if the
%microphone had been positioned 1 m from the point source in free field and
%is denoted the free field SNR.
% Input param:      H(m,l,f)      Transfer function with m number of
%                                 microphones, l loudspeakers and f frequency 
%                                 bins
%                   SNR(scalar)   Signal to noise ration in dB

if nargin < 3
    coherent = false;
end
HNoise = zeros(size(H));
Hfreefield = 1/(4*pi)*ones(size(H,3),1);
hfreefield = ifft(Hfreefield);

if coherent
    hfreefield_noisy = awgn(hfreefield,SNR, 'measured');
    for m = 1:size(H,1)  
        for l = 1:size(H,2)
            HNoise(m,l,:) = fft(hfreefield_noisy-hfreefield+ifft(squeeze(H(m,l,:))));
        end
    end
else
    for m = 1:size(H,1)  
        for l = 1:size(H,2)
            hfreefield_noisy = awgn(hfreefield,SNR, 'measured');
            HNoise(m,l,:) = fft(hfreefield_noisy-hfreefield+ifft(squeeze(H(m,l,:))));
        end
    end
end
Noise = H-HNoise;
end

