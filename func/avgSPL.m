function [Lavg] = avgSPL(p)
% Might be total trash
% Based on Equations 1.3.7 and 1.3.8 from Fundamentals
%TOTALSPL Calculates total SPL from sound pressure p across all
%frequencies, when p is of shape 1xf or fx1
prmsavg = sqrt(sum(abs(p).^2/2)/length(p));
p0 = 2e-5;
Lavg = 20*log10(prmsavg/p0);
end

