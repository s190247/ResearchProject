function [locs] = createMeasGrid(loc)
%CREATEMEASGRID Creates grid of locations according to Martins paper, where
%it says: 
% To emulate the measurement
% scenario, the room transfer functions are calculated from 8 point
% sources to 75 microphone positions (a 5 x 5 planar array in three
% different heights with 10 cm spacing between microphones5) in
% each zone
spacing_x = .1;
spacing_y = .1;
spacing_z = .1;

nx = -2:1:2;
ny = -2:1:2;
nz = -1:1:1;
it = 1;
locs = zeros(length(nx)*length(ny)*length(nz),3);
for x = 1:length(nx)
    for y = 1:length(ny)
        for z = 1:length(nz)
            locs(it,3) = loc(3)+spacing_z*nz(z);
            locs(it,2) = loc(2)+spacing_y*ny(y);
            locs(it,1) = loc(1)+spacing_x*nx(x);
            it = it+1;
        end
    end
end

end

