function [f,modes] = eigenfrequencies(dim, fmax, c) 
% Calculate Eigenfrequencies according to 3.1.5 and corresponding modes
% Modes with an even numbered index (e.g. 2,1,0) are not 0 at the middle. 
max = 20;
modes = [];
lx = dim(1);
ly = dim(2);
lz = dim(3);

for z = 0:max
    cz = (c*z/(2*lz))^2;
    for y = 0:max
        cy = (c*y/(2*ly))^2;
        for x = 0:max
            cx = (c*x/(2*lx))^2;
            f(x+1,y+1,z+1) = sqrt(cx+cy+cz);
            m = [x,y,z];
            modes = [modes;m];
        end
    end
end

% Eigenfrequencies:
fm = sortrows([f(f<fmax),modes(f<fmax,:)]);
f = fm(:,1);
modes = fm(:,2:4);
end