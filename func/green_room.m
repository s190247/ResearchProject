function [G] = green_room(p0,p,lx,ly, lz,f, n_modes, tao, c)
%GREEN_ROOM calculates Green's function in a room of 
%       dimensions lx, ly, lz [m] 
%       at given frequency bins f [Hz]
%       for a maximum number of n_modes
%       and a time constant of the modes tao (not t60!!)
%       and a speed of sound c [m/s]
%       for a receiver position p [m,m,m] and source position p0 [m,m,m]
%   Colin Benker, 26.04.2020
G = zeros(size(f));
if nargin < 9
    c = 343;
end
k = 2*pi*f/c;
for nx = 0:n_modes-1
    for ny= 0:n_modes-1
        for nz = 0:n_modes-1
            phi0 = modeshape_room(p0(1),p0(2),p0(3),nx,ny,nz,lx,ly, lz);
            phi = modeshape_room(p(1),p(2),p(3),nx,ny,nz,lx,ly, lz);

            km = sqrt( (nx*pi/lx)^2 + (ny*pi/ly)^2 + (nz*pi/lz)^2);

            G = G - 1./(lx*ly*lz) * phi0*phi ./(k.^2 - km^2 - 1j*k./(tao*c));
        end
    end
end

end

