function [phase] = group_delay(f,t)
%Calculates phase to acquire a certain group delay t [s] for a frequency
%vector f [Hz]
phase = exp(1j*2*pi*t*f);
end

