function [w] = pmFilterProp(Gexp, Gvar, pt)
%Calculates pressure matching filter according to equation 27 in Martins
%paper. Input parameters are the matrix Gexp (M,L,f) and the corresponding
%variances of each of the random variables Gvar (M,L,f). 
w = zeros(size(Gexp,2), size(Gexp,3));
for fit = 1:size(pt,2)
    Gexpf = squeeze(Gexp(:,:,fit));
    Gvarf = squeeze(Gexp(:,:,fit));
    ptf = pt(:,fit);
    Sigma = diag(sum(squeeze(Gvar(:,:,fit)),1));
    w(:,fit) = (Gexpf'*Gexpf+Sigma)^-1 * Gexpf'*ptf;
end
end

