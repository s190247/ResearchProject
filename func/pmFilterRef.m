function [w] = pmFilterRef(H, pt)
%Reference implementation of the pm filter according to equation 23 in
%Martins paper
w = zeros(size(H,2),size(H,3));
% for s = 1:size(pt,1)
for f = 1:size(pt,2)
    Hf = H(:,:,f);
    w(:,f) = (Hf'*Hf)^(-1)*Hf'*pt(:,f);
end
end

