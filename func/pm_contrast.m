function [pm_c] = pm_contrast(pr, pt)
%pm_contrast returns the pressure matching contrast (reproduction error) according to 
% equation 20, where pr is the reproduced sound field pr(m,f) and pt(m,f)
% is the target sound field. Both target and reproduced sound field should
% be of shape (m,f) with the number of microphones m and the number of
% frequency bins f. 
pm_c = sqrt(sum(abs(pt-pr).^2,1));
end

