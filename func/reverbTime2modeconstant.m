function [tao] = reverbTime2modeconstant(t60)
%REVERBTIME2MODECONSTANT Calculates tao, the time constant of a mode from a
%given reverberation time.
tao = t60/(6*log(10));
end

