function [dim,locs,locm] = setup_locations()
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
dim = [5.656, 8.609, 2.7];

locs = [0, 0, 0;
        0, dim(2)/2, 0;
        0, dim(2), 0;
        dim(1), dim(2), 0;
        dim(1), dim(2)/2, 0;
        dim(1), 0, 0;
        1.89, 4.24-0.5, 0;
        4.14, 4.35-0.5, 0]';
    
locmb = createMeasGrid([1.914, 4.24, 1.17]);
locmd = createMeasGrid([4.14, 4.35, 1.17]);
locm = [locmb; locmd]';
end

