function [c2] = speedofsound(T, p0)
% Equation 1.2.2b in Book
% gamma = ratio of specific heat constant
% p0 = static air pressure
% R = gas constant
% rho = density 
% rho = p0/(RT)

% In air:
gamma = 1.401; % Ratio
if nargin < 2
    p0 = 101.3e3; %[Pa]
end
if nargin < 1
    T = 293;
end

R = 287; %[J*kg/K]
rho = 1.204; %[kg/m^3]

% c1 and c2 should be equal!
% c1 =sqrt(gamma*p0/rho);
c2 = sqrt(gamma*R*T);
end

