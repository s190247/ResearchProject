function [tao] = t60totao(t60)
%Calculates time constant from a given reverberation time according to Lab2
%in Advanced Acoustics
tao = t60/(6*log(10));
end

