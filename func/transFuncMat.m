function [H] = transFuncMat(ps, pm, dim, f, t60)
%TRANSFUNCMAT creates transfer function matrix H according to equation 41
%in Martin's paper. 
% INPUT PARAM:
%   ps:     Position of Speakers (Sx3) Matrix
%   pm:     Position of Microphones (Mx3) Matrix
%   dim:    Dimensions of room (1x3 Vector)
% addpath(fullfile('..', 'func'))
global c rho
S = size(ps,2);  % Number of speakers
M = size(pm,2);  % Number of microphones
omega = 2*pi*f;
[feigen, modes] = eigenfrequencies(dim, max(f)+50, c);
nMod = length(modes);
% Preallocation
H = zeros(M, S, length(f)); % Preallocation of transfer function matrix
mu = zeros(nMod, length(f));
phiM = zeros(nMod, M);
phiS = zeros(nMod, S);

% Microphone mode shape Matrix phiM
for m = 1:M
    for n = 1:nMod
        phiM(n,m) = modeshape_room(pm(:,m), modes(n,:), dim);
    end
end
% Speaker mode shape Matrix phiS
for s = 1:S
    for n = 1:nMod
        phiS(n,s) = modeshape_room(ps(:,s), modes(n,:), dim);
    end
end
% mu matrix
for n = 1:nMod % Eq. 33
    omegaMod = 2*pi*feigen(n);
    if nargin < 4
        mu(n,:) = ((omegaMod/c)^2 - (omega/c).^2-eps).^(-1); % Without losses
    else
        tao = t60totao(t60);
        mu(n,:) = ((omegaMod/c)^2 - (omega/c).^2-1j*omega/(tao*c^2)).^(-1); % Including losses
    end
end

for it = 1:length(f)
    mu_diag = diag(mu(:,it));
%     H(:,:,it) = j*omega(it)*rho*phiM'*mu_diag*phiS;
    H(:,:,it) = phiM'*mu_diag*phiS;
end

H = 1/(prod(dim))*H;  % Divide by volume

end