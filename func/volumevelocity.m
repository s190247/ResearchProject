function [q] = volumevelocity(f, freefieldSPL, f_rolloff, rollofforder)
%VOLUMEVELOCITY calculates the volumevelocity of a source, given its
%frequency components f [Hz] and the desired free field SPL in 1 m
%distance. In Martins paper the sound source is supposed to emulate
%speakers with a 2nd order high-pass filter with cutoff frequency at 35 Hz.
if nargin < 4
    rollofforder = 2;
end
if nargin < 3
    f_rolloff = 0; % When not given, just assume no rolloff
end
global rho
p =  10^(freefieldSPL/20)*2e-5; % SPL to pressure
q = p*4*pi./(2*pi*f*rho*1j);
% From Martins paper: The volume velocities of the point sources in the simulations
% are scaled to produce 70 dB SPL at a distance of 1m in free field.
% A 2nd-order high-pass filter with cutoff frequency at 35 Hz is introduced
% to emulate the low frequency roll-off of a closed-box
% loudspeaker [34].

filt = designfilt('highpassiir', 'PassbandFrequency', f_rolloff/length(f), 'FilterOrder', rollofforder);
Filt = freqz(filt, length(f)+1);
Filt = Filt(2:end); % Otherwise it starts at 0Hz
q = Filt'.*q;

% Sanity Check:
% L = 20*log10(abs(j*2*pi*f.*q/(4*pi)) / 2e-5)
end

