function plotACSpeedofSound(acc11, acc12, acc21, acc22)
%PLOTACSPEEDOFSOUND Summary of this function goes here
%   Detailed explanation goes here
addpath(fullfile(pwd, 'plotting', 'util'))

figure(1)
colors = get_RGBColorCodes({'blue','red'});
colors = 1.3*colors;
plot(10*log10(abs(acc11)),'DisplayName','$H_cw_c$','Color',colors(:,1),'LineStyle',':');
hold on
plot(10*log10(abs(acc12)),'DisplayName','$H_cw_h$','Color',colors(:,1),'LineStyle','-');
plot(10*log10(abs(acc21)),'DisplayName','$H_hw_c$','Color',colors(:,2),'LineStyle','-');
plot(10*log10(abs(acc22)),'DisplayName','$H_hw_h$','Color',colors(:,2),'LineStyle',':');
grid on
legend('Location','SouthWest', 'NumColumns',2)
title('Influence of Temperature on Acoustic Contrast')
xticks([20, 50, 100, 200, 300])
yticks(-20:10:50)
xlim([20,300])
ylim([-20,59])
xlabel('Frequency [Hz]')
ylabel('Acoustic Contrast [dB]')
set(gca, 'XScale', 'log')
setLineWidth(2);
setSize([400 280]);
setFontSize([10;10;10;10]);

% print('ACHotCold','-depsc')
hold off
end

