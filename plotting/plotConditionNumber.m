function plotConditionNumber(fig, condition_number)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here
figure(fig)
plot(condition_number)
set(gca, 'YScale', 'log', 'XScale', 'log')
xlim([20,300])
title('Condition Number')
grid on
xticks([20, 50, 100, 200, 300])
% print('ConditionNumber','-dpng')
end

