function plotNoise(acNoiseFree, acc0_ic, acc20_ic, acc0_c, acc20_c)
%PLOTACSPEEDOFSOUND Summary of this function goes here
%   Detailed explanation goes here
figure(1)
colors = get_RGBColorCodes({'olive','red','grey'});
% colors(:,2) = 1.3*colors(:,2);

plot(10*log10(abs(acc0_ic)),'DisplayName','IC 0~dB','Color',colors(:,1),'LineStyle','-');
hold on
plot(10*log10(abs(acc20_ic)),'DisplayName','IC 20~dB','Color',colors(:,1),'LineStyle',':');
plot(10*log10(abs(acNoiseFree)),'DisplayName','Noise-Free','Color',colors(:,3),'LineStyle','-');

plot(10*log10(abs(acc0_c)),'DisplayName','C 0~dB','Color',colors(:,2),'LineStyle','-');
plot(10*log10(abs(acc20_c)),'DisplayName','C 20~dB','Color',colors(:,2),'LineStyle',':');

grid on
legend('Location','SouthEast', 'NumColumns',2)
title('Influence of Noise on Acoustic Contrast')
xticks([20, 50, 100, 200, 300])
yticks(-20:10:50)
xlim([20,300])
ylim([-20,59])
xlabel('Frequency [Hz]')
ylabel('Acoustic Contrast [dB]')
set(gca, 'XScale', 'log')
setLineWidth(2);
setSize([400 280]);
setFontSize([10;10;10;10]);

print('ACNoise','-depsc')
hold off
end



