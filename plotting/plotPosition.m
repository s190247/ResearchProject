function plotPosition(ac0,acm1,acm2)
%PLOTPOSITION Summary of this function goes here
%   Detailed explanation goes here
figure(2)
colors = get_RGBColorCodes({'grey','olive'});
colors = 1.3*colors;
plot(10*log10(abs(ac0)),'DisplayName','Initial Position','Color',colors(:,1));
hold on
plot(10*log10(abs(acm1)),'DisplayName','$d=0.1$~m','Color',0.8*colors(:,2));
plot(10*log10(abs(acm2)),'DisplayName','$d=0.2$~m','Color',0.6*colors(:,2));

grid on
legend('Location','SouthWest')
title('Influence of Position on Acoustic Contrast')
xticks([20, 50, 100, 200, 300])
yticks(-20:10:50)
xlim([20,300])
ylim([-20,59])
xlabel('Frequency [Hz]')
ylabel('Acoustic Contrast [dB]')
set(gca, 'XScale', 'log')
setLineWidth(2);
setSize([400 280]);
setFontSize([10;10;10;10]);

print('ACPosition','-depsc')
hold off
end

