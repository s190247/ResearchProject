function plotSNR(fig, avgSNR)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
figure(fig)
semilogx(avgSNR);
xlim([20 300]); ylim([-15 20]);xticks([20,50,100,200,300]); grid on;
title('Supposed to look like figure 5'); ylabel('SNR'); xlabel('Frequency [Hz]');
% print('SNR','-dpng')
end

