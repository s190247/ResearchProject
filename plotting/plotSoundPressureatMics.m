function plotSoundPressureatMics(fig, p_ind)
figure(1)
semilogx(squeeze(20*log10(mean(mean(abs(p_ind/2e-5),1),2))))
grid on; title('Mean SPL of all 150x8 microphone-speaker configurations'); xlim([20,300])
xticks([20,50,100,200,300])
end

