function setSize(s, fig)
%SETSIZE Summary of this function goes here
%   Detailed explanation goes here
if nargin < 2
    fig = gcf;
end
if nargin < 1
    s = [400 320];
end
set(fig, 'Position', [100, 100, s(1), s(2)])
end

