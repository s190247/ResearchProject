addpath(fullfile(pwd, 'plotting'))
addpath(fullfile(pwd, 'func'))
%% Initialise Parameters
global c rho
T = 296;
f = 1:1:300;
[dim, locs, locm] = setup_locations();
rho = 1.204; %Density air
t60 = 0.6;
c = speedofsound(T);
H0 = transFuncMat(locs, locm, dim, f, t60);
Hfull = zeros(size(H0));
w = accFilterRef(H0);
Hspeak = ones(300,1);
%% -1dB at one of the speakers
p = zeros(size(H0,1),size(H0,2));
p0 = zeros(size(H0,1),size(H0,2));
ac = zeros(size(H0,2),size(H0,3)); 
for s = 1:size(H0,2)-1
    for fit = 1:length(f)
        Hfull(:,:,fit) = H0(:,:,fit)*Hspeak(fit);
        Hfull(:,s,fit) = 10^(-1/10)*Hfull(:,s,fit);     %-1dB at one speaker

        p(:,fit) = Hfull(:,:,fit)*w(:,fit);
        p0(:,fit) = H0(:,:,fit)*w(:,fit);
    end
    ac(s,:) = acc_contrast(p(1:75,:), p(76:150,:));
end

ac0 = acc_contrast(p0(1:75,:), p0(76:150,:));

%% Phase mismatch
p = zeros(size(H0,1),size(H0,2));
p0 = zeros(size(H0,1),size(H0,2));
acp = zeros(size(H0,2),size(H0,3));
delay = 2e-3;
phase_mismatch = group_delay(f,delay);
for s = 1:size(H0,2)
    for fit = 1:length(f)
        Hfull(:,:,fit) = H0(:,:,fit)*Hspeak(fit);
        Hfull(:,s,fit) = phase_mismatch(fit).*Hfull(:,s,fit);     

        p(:,fit) = Hfull(:,:,fit)*w(:,fit);
        p0(:,fit) = H0(:,:,fit)*w(:,fit);
    end
    acp(s,:) = acc_contrast(p(1:75,:), p(76:150,:));
end




plotSpeaker(ac0, mean(ac,1), mean(acp,1)');
mean(10*log10(ac0))
mean(10*log10(mean(ac,1)))
mean(10*log10(mean(acp,1)))