addpath(fullfile(pwd, 'func'))
addpath(fullfile(pwd, 'plotting'))
%% Initialise parameters
global c rho
c = 343;
f = 1:1:300;
[dim, locs, locm] = setup_locations();
rho = 1.204; %Density air
t60 = 0.6;
H = transFuncMat(locs, locm, dim, f, t60);
freefieldSPL = 70;      %SPL point sources would radiate at 1m when in free field
q = volumevelocity(f, freefieldSPL, 35, 2);

%% ACC Monte-Carlo noise
w_acc = accFilterRef(H);
pr_accNoise = zeros(size(H,1),size(H,2));
pr_acc = zeros(size(pr_accNoise));
monte_carlo_it = 100;
SNR = [0; 20; 0; 20];
coherent = [false; false; true; true];
accNoise = zeros(length(SNR),monte_carlo_it, length(f));
meanAccNoise = zeros(length(SNR), length(f));
for i = 1:length(SNR)
    for mcit = 1:monte_carlo_it
        [HNoise, ~] = addwgn2H(H,SNR(i), coherent(i));
        w_accN = accFilterRef(HNoise);
        for fit = 1:length(f)
            pr_acc(:,fit) = H(:,:,fit)*w_acc(:,fit);
            pr_accNoise(:,fit) = H(:,:,fit)*w_accN(:,fit);
        end
        accNoise(i,mcit,:) = acc_contrast(pr_accNoise(1:75,:), pr_accNoise(76:150,:));
    end
end

acNoiseFree = acc_contrast(pr_acc(1:75,:),pr_acc(76:150,:));

ac0_ic = mean(squeeze(accNoise(1,:,:)),1);  %0 dB SNR, Incoherent
ac20_ic = mean(squeeze(accNoise(2,:,:)),1);  %20 dB SNR, Incoherent
ac0_c = mean(squeeze(accNoise(3,:,:)),1);  %0 dB SNR, Coherent
ac20_c = mean(squeeze(accNoise(4,:,:)),1);  %20 dB SNR, Coherent

%%
plotNoise(acNoiseFree, ac0_ic, ac20_ic, ac0_c, ac20_c);
mean(10*log10(ac0_ic))
mean(10*log10(ac20_ic))
mean(10*log10(ac0_c))
mean(10*log10(ac20_c))