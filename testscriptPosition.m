addpath(fullfile(pwd, 'func'))
addpath(fullfile(pwd, 'plotting'))
%% Initialise parameters
global c rho
T = 296;
f = 1:1:300;
[dim, locs, locm] = setup_locations();
rho = 1.204; %Density air
t60 = 0.6;
c = speedofsound(T);
H0 = transFuncMat(locs, locm, dim, f, t60);
w = accFilterRef(H0);
p0 = zeros(size(H0,1),size(H0,3));

for fit = 1:length(f)
    p0(:,fit) = H0(:,:,fit)*w(:,fit);
end
ac0 = acc_contrast(p0(1:75,:),p0(76:150,:));
%% Change microphone position 10cm in each direction and then take mean
d_vec = [0.1,0.2];
for d_it = 1:length(d_vec)
    d = d_vec(d_it);
    add_pos = [d,d,d;
           d,d,-d;
           d,-d,d;
           d,-d,-d;
           -d,d,d;
           -d,d,-d;
           -d,-d,d;
           -d,-d,-d]';

    for i = 1:size(add_pos,2)
        new_loc{i} = locm+add_pos(:,i);
        new_H{i} = transFuncMat(locs, new_loc{i}, dim, f, t60);
        for fit = 1:length(f)
            p{i}(:,fit) = new_H{i}(:,:,fit)*w(:,fit);
        end
        ac{i} = acc_contrast(p{i}(1:75,:),p{i}(76:150,:));
    end
    acm(d_it,:) = mean(cell2mat(ac'));    
end

%% Move speaker
d_vec = 0.1;
new_H = {};
ac = {};
for d_it = 1:length(d_vec)
    d = d_vec(d_it);
    add_pos = [d,d,d;
               d,d,-d;
               d,-d,d;
               d,-d,-d;
               -d,d,d;
               -d,d,-d;
               -d,-d,d;
               -d,-d,-d]';
    for i = 1:size(add_pos,2)

        loc_new = locs+[0;d;0];
        new_H{i} = transFuncMat(loc_new, locm, dim, f, t60);
        for fit = 1:length(f)
            ps(:,fit) = new_H{i}(:,:,fit)*w(:,fit);
        end
        ac{i} = acc_contrast(ps(1:75,:),ps(76:150,:));
    end
    acsm(d_it,:) = mean(cell2mat(ac'));    
end
%%
plotPosition(ac0,acm(1,:),acm(2,:));
mean(10*log10(abs(ac0)))
mean(10*log10(abs(acm(1,:))))
mean(10*log10(abs(acm(2,:))))