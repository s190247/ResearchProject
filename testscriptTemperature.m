addpath(fullfile(pwd, 'func'))
addpath(fullfile(pwd, 'plotting'))
%% Initialise parameters
global c rho
T1 = 293;
T2 = 299;
f = 1:1:300;
[dim, locs, locm] = setup_locations();
rho = 1.204; %Density air
t60 = 0.6;
c = speedofsound(T1);
H1 = transFuncMat(locs, locm, dim, f, t60);
c = speedofsound(T2);
H2 = transFuncMat(locs, locm, dim, f, t60);
freefieldSPL = 70;      %SPL point sources would radiate at 1m when in free field
q = volumevelocity(f, freefieldSPL, 35, 2);

%% 
w1 = accFilterRef(H1);
w2 = accFilterRef(H2);
p11 = zeros(size(H1,1),size(H1,3));
p12 = p11; p21 = p11; p22 = p11;

for fit = 1:length(f)
    p11(:,fit) = H1(:,:,fit)*w1(:,fit);
    p12(:,fit) = H1(:,:,fit)*w2(:,fit);
    p21(:,fit) = H2(:,:,fit)*w1(:,fit);
    p22(:,fit) = H2(:,:,fit)*w2(:,fit);
end
acc11 = acc_contrast(p11(1:75,:),p11(76:150,:));
acc12 = acc_contrast(p12(1:75,:),p12(76:150,:));
acc21 = acc_contrast(p21(1:75,:),p21(76:150,:));
acc22 = acc_contrast(p22(1:75,:),p22(76:150,:));

plotACSpeedofSound(acc11, acc12, acc21, acc22);
mean(10*log10(abs(acc12)))
mean(10*log10(abs(acc21)))